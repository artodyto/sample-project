﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Brands.Data
{
    public class Brand
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("code")]
        [Required]
        public string code { get; set; }

        [JsonProperty("description")]
        [Required]
        public string description { get; set; }
    }
}
